﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace footlucker.Models
{
    public class BranchesSuppliers
    {
        [Key]
        public int Id { get; set; }
        public int BrancheId  { get; set; }
        public virtual Branch Branch { get; set; }
        public int SupplierId { get; set; }
        public virtual Supplier Supplier { get; set; }
    }
}