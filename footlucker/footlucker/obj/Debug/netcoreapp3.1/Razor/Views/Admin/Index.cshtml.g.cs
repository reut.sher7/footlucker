#pragma checksum "C:\Users\HP\Documents\לימודים\אפליקציות\footlucker\footlucker\footlucker\Views\Admin\Index.cshtml" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "30a1faebcf8d52ad350de3537ae7905727032626"
// <auto-generated/>
#pragma warning disable 1591
[assembly: global::Microsoft.AspNetCore.Razor.Hosting.RazorCompiledItemAttribute(typeof(AspNetCore.Views_Admin_Index), @"mvc.1.0.view", @"/Views/Admin/Index.cshtml")]
namespace AspNetCore
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.Rendering;
    using Microsoft.AspNetCore.Mvc.ViewFeatures;
#nullable restore
#line 1 "C:\Users\HP\Documents\לימודים\אפליקציות\footlucker\footlucker\footlucker\Views\_ViewImports.cshtml"
using footlucker;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\HP\Documents\לימודים\אפליקציות\footlucker\footlucker\footlucker\Views\_ViewImports.cshtml"
using footlucker.Models;

#line default
#line hidden
#nullable disable
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"30a1faebcf8d52ad350de3537ae7905727032626", @"/Views/Admin/Index.cshtml")]
    [global::Microsoft.AspNetCore.Razor.Hosting.RazorSourceChecksumAttribute(@"SHA1", @"6ab9bf4575635aad11c7786cfc35ac86f81bcadd", @"/Views/_ViewImports.cshtml")]
    public class Views_Admin_Index : global::Microsoft.AspNetCore.Mvc.Razor.RazorPage<dynamic>
    {
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_0 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/jquery/dist/jquery.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.SingleQuotes);
        private static readonly global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute __tagHelperAttribute_1 = new global::Microsoft.AspNetCore.Razor.TagHelpers.TagHelperAttribute("src", new global::Microsoft.AspNetCore.Html.HtmlString("~/lib/d3/d3.js"), global::Microsoft.AspNetCore.Razor.TagHelpers.HtmlAttributeValueStyle.DoubleQuotes);
        #line hidden
        #pragma warning disable 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperExecutionContext __tagHelperExecutionContext;
        #pragma warning restore 0649
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner __tagHelperRunner = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperRunner();
        #pragma warning disable 0169
        private string __tagHelperStringValueBuffer;
        #pragma warning restore 0169
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __backed__tagHelperScopeManager = null;
        private global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager __tagHelperScopeManager
        {
            get
            {
                if (__backed__tagHelperScopeManager == null)
                {
                    __backed__tagHelperScopeManager = new global::Microsoft.AspNetCore.Razor.Runtime.TagHelpers.TagHelperScopeManager(StartTagHelperWritingScope, EndTagHelperWritingScope);
                }
                return __backed__tagHelperScopeManager;
            }
        }
        private global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper;
        #pragma warning disable 1998
        public async override global::System.Threading.Tasks.Task ExecuteAsync()
        {
            WriteLiteral(@"<div class='page_title_container'>
    <h2 class='page_title'>סטטיסטיקות למנהל</h2>
</div>


<div class='admin_page_container'>
    <div class='admin_twitt_container'>
        <div class='twitt_label'>צייץ: </div>
        <a class=""twitter-share-button""
           href=""https://twitter.com/intent/tweet?text=""
           data-size=""large"">
        </a>
    </div>

    <div class='admin_graph_container'>
        <div class='admin_graph' id='graph_1'>
            <h1>מכירות של מוצרים בכל סניף</h1>
            <div id=""stat_1""></div>
        </div>
        <div class='admin_graph' id='graph_2'>
            <h1>מכירות לפי סוגי מוצרים</h1>
            <div id=""stat_2""></div>
        </div>
    </div>
</div>


");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "30a1faebcf8d52ad350de3537ae79057270326264613", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_0);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral("\r\n");
            __tagHelperExecutionContext = __tagHelperScopeManager.Begin("script", global::Microsoft.AspNetCore.Razor.TagHelpers.TagMode.StartTagAndEndTag, "30a1faebcf8d52ad350de3537ae79057270326265652", async() => {
            }
            );
            __Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper = CreateTagHelper<global::Microsoft.AspNetCore.Mvc.Razor.TagHelpers.UrlResolutionTagHelper>();
            __tagHelperExecutionContext.Add(__Microsoft_AspNetCore_Mvc_Razor_TagHelpers_UrlResolutionTagHelper);
            __tagHelperExecutionContext.AddHtmlAttribute(__tagHelperAttribute_1);
            await __tagHelperRunner.RunAsync(__tagHelperExecutionContext);
            if (!__tagHelperExecutionContext.Output.IsContentModified)
            {
                await __tagHelperExecutionContext.SetOutputContentAsync();
            }
            Write(__tagHelperExecutionContext.Output);
            __tagHelperExecutionContext = __tagHelperScopeManager.End();
            WriteLiteral(@"
<script>
    window.twttr = (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0],
            t = window.twttr || {};
        if (d.getElementById(id)) return t;
        js = d.createElement(s);
        js.id = id;
        js.src = ""https://platform.twitter.com/widgets.js"";
        fjs.parentNode.insertBefore(js, fjs);

        t._e = [];
        t.ready = function (f) {
            t._e.push(f);
        };

        return t;
    }(document, ""script"", ""twitter-wjs""));</script>
<script>
    $.ajax({
        type: 'GET',
        url: '/Admin/BranchSales',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: (res) => {
            createGraphWithData('#stat_1', res);
            //createPieChartWithData('#stat_1', res);
        },
    });

    $.ajax({
        type: 'GET',
        url: '/Admin/ProductSales',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: (res)");
            WriteLiteral(@" => createGraphWithData('#stat_2', res),
    });

    function createGraphWithData(element, data) {
        console.log(data);
        var colors = ['#F44336', '#5C6BC0', '#1E88E5', '#4DD0E1', '#0288D1', '#00897B'];
        var margin = ({ top: 20, right: 0, bottom: 30, left: 40 });
        var height = 300;
        var width = 430;
        var yAxis = g => g
            .attr(""transform"", `translate(${margin.left},0)`)
            .call(d3.axisLeft(y))
            .call(g => g.select("".domain"").remove());
        var xAxis = g => g
            .attr(""transform"", `translate(0,${height - margin.bottom})`)
            .call(d3.axisBottom(x)
                .tickSizeOuter(0));
        var y = d3.scaleLinear()
            .domain([0, d3.max(data, d => d.count)]).nice()
            .range([height - margin.bottom, margin.top]);
        var x = d3.scaleBand()
            .domain(data.map(d => d.name))
            .range([margin.left, width - margin.right])
            .padding(0.1);

       ");
            WriteLiteral(@" var svg = d3.select(element)
            .append('svg')
            .attr('width', width)
            .attr('height', height);

        svg.append(""g"")
            .attr(""fill"", ""steelblue"")
            .selectAll(""rect"").data(data).enter().append(""rect"")
            .attr(""x"", d => x(d.name))
            .attr(""y"", d => y(d.count))
            .attr(""height"", d => y(0) - y(d.count))
            .attr(""width"", x.bandwidth())
            .attr(""fill"", (d, i) => { return colors[i % colors.length] });

        svg.append(""g"")
            .call(xAxis);

        svg.append(""g"")
            .call(yAxis);

        svg.node();

        $('.admin_page_container').css('visibility', 'visible');
        $('.admin_page_container').css('animation-name', 'fade');
    }

    function createPieChartWithData(element, data) {

        var pie = d3.pie()
            .sort(null)
            .value(d => d.count);

        var radius = Math.min(width, height) / 2;

        var arcLabel = d3.arc().");
            WriteLiteral(@"innerRadius(radius).outerRadius(radius);

        var arc = d3.arc()
            .innerRadius(0)
            .outerRadius(Math.min(width, height) / 2 - 1);

        var width = 500;

        var height = Math.min(width, 500);

        var color = d3.scaleOrdinal()
            .domain(data.map(d => d.name))
            .range(d3.quantize(t => d3.interpolateSpectral(t * 0.8 + 0.1), data.length).reverse());

        var arcs = pie(data);

        var svg = d3.select('#stat_3')
            .attr(""text-anchor"", ""middle"")
            .style(""font"", ""12px sans-serif"");

        var g = svg.append(""g"")
            .attr(""transform"", `translate(${width / 2},${height / 2})`);

        g.selectAll(""path"")
            .data(arcs)
            .enter().append(""path"")
            .attr(""fill"", d => color(d.data.name))
            .attr(""stroke"", ""white"")
            .attr(""d"", arc)
            .append(""title"")
            .text(d => `${d.data.name}: ${d.data.count.toLocaleString()}`);

      ");
            WriteLiteral(@"  var text = g.selectAll(""text"")
            .data(arcs)
            .enter().append(""text"")
            .attr(""transform"", d => `translate(${arcLabel.centroid(d)})`)
            .attr(""dy"", ""0.35em"");

        text.append(""tspan"")
            .attr(""x"", 0)
            .attr(""y"", ""-0.7em"")
            .style(""font-weight"", ""bold"")
            .text(d => d.data.name);

        text.filter(d => (d.endAngle - d.startAngle) > 0.25).append(""tspan"")
            .attr(""x"", 0)
            .attr(""y"", ""0.7em"")
            .attr(""fill-opacity"", 0.7)
            .text(d => d.data.count.toLocaleString());

        svg.node();
    }
</script>
");
        }
        #pragma warning restore 1998
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.ViewFeatures.IModelExpressionProvider ModelExpressionProvider { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IUrlHelper Url { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.IViewComponentHelper Component { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IJsonHelper Json { get; private set; }
        [global::Microsoft.AspNetCore.Mvc.Razor.Internal.RazorInjectAttribute]
        public global::Microsoft.AspNetCore.Mvc.Rendering.IHtmlHelper<dynamic> Html { get; private set; }
    }
}
#pragma warning restore 1591
