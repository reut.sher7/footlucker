﻿using footlucker.Models;
using Microsoft.EntityFrameworkCore;

namespace footlucker.Data
{
    public class ProjectContext : DbContext
    {
        public DbSet<Product> Products { get; set; }
        public DbSet<Branch> Branches { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Purchase> Purchases { get; set; }
        public DbSet<Supplier> Suppliers { get; set; }
        public DbSet<ProductType> ProductTypes { get; set; }
        public DbSet<BranchesSuppliers> BranchesSuppliers { get; set; }
        public ProjectContext(DbContextOptions<ProjectContext> options)
            : base(options)
        { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
                .HasOne(p => p.Supplier)
                .WithMany(b => b.Products)
                .HasForeignKey(p => p.SupplierId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Product>()
                .HasOne(p => p.ProductType)
                .WithMany(b => b.Products)
                .HasForeignKey(p => p.ProductTypeId);

            modelBuilder.Entity<Purchase>()
                .HasOne(p => p.Branch)
                .WithMany(b => b.Purchases)
                .HasForeignKey(p => p.BranchId);

            modelBuilder.Entity<Purchase>()
                .HasOne(p => p.User)
                .WithMany(b => b.Purchases)
                .HasForeignKey(p => p.UserId);

            modelBuilder.Entity<BranchesSuppliers>()
               .HasOne(p => p.Branch)
               .WithMany(b => b.BranchesSuppliers)
               .HasForeignKey(p => p.BrancheId);

            modelBuilder.Entity<BranchesSuppliers>()
                .HasOne(b => b.Supplier)
                .WithMany(s => s.BranchesSuppliers)
                .HasForeignKey(p => p.SupplierId);
        }
    }
}
