﻿using footlucker.Models;
using System;
using System.Linq;

namespace footlucker.Data
{
    public static class DbInitializer
    {
        public static void Initialize(ProjectContext context)
        {

            var users = new User[]
                {
                    new User{Username="admin",Password="admin",Address="some admin adsress",IsAdmin=true,Gender=false},
                    new User{Username="user1",Password="user1",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user2",Password="user2",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user3",Password="user3",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user4",Password="user4",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user5",Password="user5",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user6",Password="user6",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user7",Password="user7",Address="some user adsress",IsAdmin=false,Gender=false},
                    new User{Username="user8",Password="user8",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user9",Password="user9",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user10",Password="user10",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user11",Password="user11",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user12",Password="user12",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user13",Password="user13",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user14",Password="user14",Address="some user adsress",IsAdmin=false,Gender=true},
                    new User{Username="user15",Password="user15",Address="some user adsress",IsAdmin=false,Gender=true}
                };
            var branches = new Branch[]
                {
                    new Branch{Name="סניף כפר סבא",Lat=32.176,Long=34.894,City="כפר סבא",Address="כתובת בכפר סבא",Telephone="0586417813",IsSaturday=true},
                    new Branch{Name="סניף נמל תל אביב",Lat=32.08,Long=34.77,City="תל אביב",Address="כתובת נמל תל אביב",Telephone="0586417813",IsSaturday=false},
                    new Branch{Name="סניף דיזינגוף תל אביב",Lat=32.078,Long=34.77,City="תל אביב",Address="כתובת דיזינגוף תל אביב",Telephone="0586417813",IsSaturday=true},
                    new Branch{Name="סניף חולון",Lat=32.017,Long=34.78,City="חולון",Address="כתובת בחולון",Telephone="0586417813",IsSaturday=false},
                    new Branch{Name="סניף בת ים",Lat=32.015,Long=34.753,City="בת ים",Address="כתובת בבת ים",Telephone="0586417813",IsSaturday=true},
                };
            var suppliers = new Supplier[]
                {
                    new Supplier{Name="NIKE"},
                    new Supplier{Name="ADIDAS"},
                    new Supplier{Name="VANS"},
                };

            var branchesSuppliers = new BranchesSuppliers[]
               {
                    new BranchesSuppliers{BrancheId=1,SupplierId=1},
                    new BranchesSuppliers{BrancheId=2,SupplierId=2},
                    new BranchesSuppliers{BrancheId=3,SupplierId=3},

               };
            var productTypes = new ProductType[]
            {
                new ProductType{Gender=false,Name="AIR JORDAN MEN"},
                new ProductType{Gender=true,Name="AIR JORDAN WOMEN"},
                new ProductType{Gender=false,Name="ORIGINALS MEN"},
                new ProductType{Gender=true,Name="ORIGINALS WOMEN"},
                new ProductType{Gender=false,Name="OLD SKOOL MEN"},
                new ProductType{Gender=true,Name="OLD SKOOL"}
            };
            var products = new Product[]
                {
                    new Product{Name="Air jordan low  men 1",Size=35,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan low  men 2",Size=35,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan low men 3",Size=36,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan low men 4",Size=37,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan low men 5",Size=38,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },

                    new Product{Name="Air jordan mid men 1",Size=46,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan mid men 2",Size=45,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan mid men 3",Size=47,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan mid men 4",Size=47,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },
                    new Product{Name="Air jordan mid men 5",Size=45,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[0] },

                    new Product{Name="Air jordan low  women 1",Size=35,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan low  women 2",Size=35,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan low women 3",Size=36,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan low women 4",Size=37,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan low women 5",Size=38,Price=51,PictureName="jordanlow.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },

                    new Product{Name="Air jordan mid women 1",Size=46,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan mid women 2",Size=45,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan mid women 3",Size=47,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan mid women 4",Size=47,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },
                    new Product{Name="Air jordan mid women 5",Size=45,Price=51,PictureName="jordanmid.jpg", Supplier = suppliers[0] ,ProductType = productTypes[1] },


                    new Product{Name="Originals low men 1",Size=35,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals low men 2",Size=36,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals low men 3",Size=36,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals low men 4",Size=37,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals low men 5",Size=38,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[2] },

                    new Product{Name="Originals mid men 1",Size=45,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals mid men 2",Size=46,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals mid men 3",Size=47,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals mid men 4",Size=47,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[2] },
                    new Product{Name="Originals mid men 5",Size=48,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[2] },

                    new Product{Name="Originals low women 1",Size=35,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals low women 2",Size=36,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals low women 3",Size=36,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals low women 4",Size=37,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals low women 5",Size=38,Price=51,PictureName="originalslow.jpg", Supplier = suppliers[1] ,ProductType = productTypes[3] },

                    new Product{Name="Originals mid women 1",Size=45,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals mid women 2",Size=46,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals mid women 3",Size=47,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals mid women 4",Size=47,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[3] },
                    new Product{Name="Originals mid women 5",Size=48,Price=51,PictureName="originalsmid.jpeg", Supplier = suppliers[1] ,ProductType = productTypes[3] },

                    new Product{Name="Old skool low men 1",Size=35,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool low men 2",Size=35,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool low men 3",Size=36,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool low men 4",Size=36,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool low men 5",Size=37,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },

                    new Product{Name="Old skool mid men 1",Size=45,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool mid men 2",Size=46,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool mid men 3",Size=47,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool mid men 4",Size=47,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },
                    new Product{Name="Old skool mid men 5",Size=48,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[4] },

                    new Product{Name="Old skool low women 1",Size=35,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool low women 2",Size=35,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool low women 3",Size=36,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool low women 4",Size=36,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool low women 5",Size=37,Price=51,PictureName="vanslow.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },

                    new Product{Name="Old skool mid women 1",Size=45,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool mid women 2",Size=46,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool mid women 3",Size=47,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool mid women 4",Size=47,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                    new Product{Name="Old skool mid women 5",Size=48,Price=51,PictureName="vansmid.jpg", Supplier = suppliers[2] ,ProductType = productTypes[5] },
                };
            var Purchases = new Purchase[]
                {
					// admin
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[0],Branch=branches[0],User=users[0]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[1],Branch=branches[1],User=users[0]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[20],Branch=branches[2],User=users[0]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[21],Branch=branches[3],User=users[0]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[40],Branch=branches[4],User=users[0]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[41],Branch=branches[0],User=users[0]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[2],Branch=branches[1],User=users[1]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[3],Branch=branches[2],User=users[1]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[22],Branch=branches[3],User=users[1]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[23],Branch=branches[4],User=users[1]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[42],Branch=branches[0],User=users[1]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[43],Branch=branches[1],User=users[1]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[3],Branch=branches[2],User=users[2]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[4],Branch=branches[3],User=users[2]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[23],Branch=branches[4],User=users[2]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[24],Branch=branches[0],User=users[2]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[43],Branch=branches[1],User=users[2]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[44],Branch=branches[2],User=users[2]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[0],Branch=branches[3],User=users[3]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[4],Branch=branches[4],User=users[3]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[20],Branch=branches[0],User=users[3]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[24],Branch=branches[1],User=users[3]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[40],Branch=branches[2],User=users[3]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[44],Branch=branches[3],User=users[3]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[5],Branch=branches[0],User=users[4]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[6],Branch=branches[0],User=users[4]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[25],Branch=branches[0],User=users[4]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[26],Branch=branches[0],User=users[4]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[45],Branch=branches[0],User=users[4]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[46],Branch=branches[0],User=users[4]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[7],Branch=branches[0],User=users[5]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[8],Branch=branches[0],User=users[5]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[27],Branch=branches[0],User=users[5]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[28],Branch=branches[0],User=users[5]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[47],Branch=branches[0],User=users[5]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[48],Branch=branches[0],User=users[5]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[8],Branch=branches[0],User=users[6]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[9],Branch=branches[0],User=users[6]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[28],Branch=branches[0],User=users[6]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[29],Branch=branches[0],User=users[6]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[48],Branch=branches[0],User=users[6]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[49],Branch=branches[0],User=users[6]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[5],Branch=branches[0],User=users[7]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[9],Branch=branches[0],User=users[7]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[25],Branch=branches[0],User=users[7]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[29],Branch=branches[0],User=users[7]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[45],Branch=branches[0],User=users[7]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[49],Branch=branches[0],User=users[7]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[10],Branch=branches[0],User=users[8]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[11],Branch=branches[0],User=users[8]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[30],Branch=branches[0],User=users[8]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[31],Branch=branches[0],User=users[8]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[50],Branch=branches[0],User=users[8]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[51],Branch=branches[0],User=users[8]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[12],Branch=branches[1],User=users[9]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[13],Branch=branches[1],User=users[9]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[32],Branch=branches[1],User=users[9]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[33],Branch=branches[1],User=users[9]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[52],Branch=branches[1],User=users[9]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[53],Branch=branches[1],User=users[9]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[13],Branch=branches[2],User=users[10]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[14],Branch=branches[2],User=users[10]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[33],Branch=branches[2],User=users[10]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[34],Branch=branches[2],User=users[10]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[53],Branch=branches[2],User=users[10]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[54],Branch=branches[2],User=users[10]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[10],Branch=branches[3],User=users[11]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[14],Branch=branches[3],User=users[11]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[30],Branch=branches[3],User=users[11]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[34],Branch=branches[3],User=users[11]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[50],Branch=branches[3],User=users[11]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[54],Branch=branches[3],User=users[11]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[15],Branch=branches[4],User=users[12]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[16],Branch=branches[4],User=users[12]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[35],Branch=branches[4],User=users[12]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[36],Branch=branches[4],User=users[12]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[55],Branch=branches[4],User=users[12]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[56],Branch=branches[4],User=users[12]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[17],Branch=branches[1],User=users[13]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[18],Branch=branches[1],User=users[13]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[37],Branch=branches[1],User=users[13]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[38],Branch=branches[1],User=users[13]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[57],Branch=branches[1],User=users[13]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[58],Branch=branches[1],User=users[13]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[18],Branch=branches[0],User=users[14]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[19],Branch=branches[0],User=users[14]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[38],Branch=branches[0],User=users[14]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[39],Branch=branches[0],User=users[14]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[58],Branch=branches[0],User=users[14]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[59],Branch=branches[0],User=users[14]},
					
					new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[15],Branch=branches[2],User=users[15]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[19],Branch=branches[2],User=users[15]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[35],Branch=branches[2],User=users[15]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[39],Branch=branches[2],User=users[15]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[55],Branch=branches[2],User=users[15]},
                    new Purchase{Count=3,PurchaseDate=new DateTime(2018,9,22),Product=products[59],Branch=branches[2],User=users[15]},
                };

            // Look for any Users.
            if (!context.Users.Any())
            {
                foreach (User u in users)
                {
                    context.Users.Add(u);
                }
                context.SaveChanges();
            }

            // Look for any Branches.
            if (!context.Branches.Any())
            {
                foreach (Branch c in branches)
                {
                    context.Branches.Add(c);
                }
                context.SaveChanges();
            }

            // Look for any Suppliers.
            if (!context.Suppliers.Any())
            {
                foreach (Supplier s in suppliers)
                {
                    context.Suppliers.Add(s);
                }
                context.SaveChanges();
            }


            // Look for any Suppliers.
            if (!context.BranchesSuppliers.Any())
            {
                foreach (BranchesSuppliers bs in branchesSuppliers)
                {
                    context.BranchesSuppliers.Add(bs);
                }
                context.SaveChanges();
            }

            

            // Look for any ProductTypes.
            if (!context.ProductTypes.Any())
            {
                foreach (ProductType u in productTypes)
                {
                    context.ProductTypes.Add(u);
                }
                context.SaveChanges();
            }

            // Look for any Products.
            if (!context.Products.Any())
            {
                foreach (Product e in products)
                {
                    context.Products.Add(e);
                }

                context.SaveChanges();
            }

            // Look for any Products.
            if (!context.Purchases.Any())
            {
                foreach (Purchase p in Purchases)
                {
                    context.Purchases.Add(p);
                }
                context.SaveChanges();
            }

            return;
        }
    }
}